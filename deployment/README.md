## Introduction
  This recipe is made available to help in simulating the case of the best practice to implement the NEW ECC on BareMetal platform on k3s using Virtual or Physical Machines.

## Prerequisites for linux system:
 - Your system should be sudo passwordless access.
 - or sudo minimal requirement [k3s sudo Prerequisites](https://gitlab.com/progressoft/devops/reliability/k3s/-/blob/master/docs/sudo-commands) 

# Installation for Prmpt-check: 

Below are the steps for full deployment for this application on a k3s cluster: 

## A- vagrant instulation: 
  1. Apply below script:  
  ```bash
      wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg \n
      echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | $ sudo tee /etc/apt/sources.list.d/hashicorp.list \n
      sudo apt update && sudo apt install vagrant \n
      vagrant --version
      vagrant plugin install vagrant-env
  ```
  2. Make you vagrant instance as ``` ssh key based authentication and sudo passwordless access ```.

## B- K3S offline deployment:

  1. Download latest k3s deploymet package from
    [k3s gitlab releases](https://gitlab.com/progressoft/devops/reliability/k3s/-/releases) page, and follow the steps in README.md

  2. Below some extra points for k3s cluster for new ECC: 
   
   ```bash
       sudo chmod 0600 k3s/vagrant.pem # 0- change the file permission to -rw-------.
       vim k3s/k3s-env # 1- edit k3s-env file and replace the MASTERS_IPS with your nodes IP addresses separated by space of vagrant subnet range.
       vim k3s/k3s-env # 2- change the SSH_USER to vagrant if not present.
       vim k3s/k3s-env # 3- remove any app that you do not want in the script. For New ECC to install add these APPS=("istio" "istio-monitoring" "signoz").
       vim k3s/k3s-env  # 4- save this k3s-env file.
       vagrant plugin install k3s/k3s-env # 5- load the k3s-env plugins to prepare vagrant.
       cd k3s/vagrant/ # 6- change this directory to start the vagrant instances.
       vagrant up # 7- run this command to start the nodes.
       sudo bash ../k3s-run.sh  # 8- run this command it will install k3s service across all the nodes also push the images.
       ssh vagrant@192.168.56.11 -t  'sudo systemctl status k3s.service' # 9- once step 8 is done, check k3s service and make sure its running.
       sudo bash ../k3s-run-app.sh # 10- run this command to install the apps and provide your password everytime it requires that.
       cp ../k3s-kubeconfig /home/$USER/.kube/config # 11- once everything is fine, use this to copy command to copy the config file to your home directory, and make sure that you have created .kube folder in case its not created then ( mkdir -p /home/$USER/.kube ).
       export KUBECONFIG=/home/$USER/.kube/config # 12- run this command will export the config file so kubectl will identify the cluster.
       kubectl get nodes, kubectl get pods -n kube-system  # 13- test kubectl command and try to create a first pod (kubectl run nginx --image=nginx:alpine) and kubectl get pods -n default to check if the pod is running.
   ```

## C- Update the Istio TLS Self Certificate:
 ###  Certificate Installation:
 - Note that we use self signed certifacte just as example at bank site you need to request this.
 - Go to the ``` 00-tls-self-signed-certifacte ``` path and run the ``` install.sh ```.
 - This install.sh script will create a new secret call istio-ingressgateway-certs.
 - Then once the sercet is created it will restart the istio pods as well.
 ```bash
     cd 00-tls-self-signed-certifacte/
     bash install.sh
 ```

## D- Nginx Load Balancer:
 ###  Install and configure nginx LB:
 - Note that this is just at local PC and bank site we need to ask them to add k3s node with istio port at F5 or any loadbalcer host.
 - Go to the ``` 01-nginx-as-load-balance-local-pc ``` path and run the ``` install.sh ```.
 - Change the IP variable to your IP address.
 - Change the nodeIP variable to your (vagrant IP or host node IP).
 - This install.sh script will do 3 tasks ``` it will add hostname in /etc/hosts ```, ``` it will install nginx```, and ``` it will update the nginx.conf ```.
 ```bash
     cd ../01-nginx-as-load-balance-local-pc/
     bash install.sh
 ```

## E- Download the image to Local System:
 ###  Save images and comprese:
 - Go to the ``` 02-save-images-at-office-with-net ``` path, and update the image tags under ``` images-list.txt ```.
 - Then run the ``` save-images.sh ```.
 - This save-images.sh script will pull all the images which is mentioned in the images-list.txt and comprese all the saved images.
 ```bash
     cd ../02-save-images-at-office-with-net/
     bash save-images.sh
 ```

## F- Import images to Node:
 ###  import images to Vagrant HostNode:
 - Go to the ``` 03-import-images-at-bank-side ``` path.
 - Change the IP variable to your (vagrant IP or host node IP).
 - Then run the ``` import.sh ```.
 - This import.sh script will push all the images into vagrant or node system and it will import them into Containerd runtime on the instances.
 ```bash
     cd ../03-import-images-at-bank-side/
     bash import.sh
 ``` 

## G- Hashicorp vault ( latest version):

 ###  Vault Installation: 
 - Go to ``` 04-vault ``` path and follow the installation steps under ``` install.sh ```.
 ```bash
     cd ../04-vault/
     cat install.sh ## Follow the steps one by one
 ```
## H- Database Deployment ( MSSQL ): 
 - Note that this just for simulate the case at bank site we will not used docker or helm for database part and the database can decide.
 - in this example we use MSSQL you can use oracle as well. 
 - We can use DBSeaver as the MSSQL client software.
 - There are two ways to deploy an MSSQL database, using a Docker container or a Helm chart. Additionally, we will be using DBeaver as the MSSQL client software (docker or helm): 

 ### 1. Docker image For MSSQL:

 - Execute ```docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=P@ssw0rd" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2022-latest```
 - Execute ```docker inspect <CONTAINER ID>``` to get the container IP  

 ### 2. or Helm chart: 
    
   * Execute ```helm install mssql-latest-deploy . --set ACCEPT_EULA.value=Y --set MSSQL_PID.value=Developer```


## I- Charts deployments: 
 ### Download helm chart: 
 - Go to ``` 05-download-helmchart ``` path and follow the installation steps under ``` runt.sh ```.
 ```bash
     cd ../05-download-helmchart/
     cat runt.sh  ## Follow the steps one by one
 ```
## J- Known Issues:
 ### 1. Extra configuration for applications: 
- Go to ``` 06-known-issues ``` path and follow the configuration steps for each issues.
 ```bash
     cd ../06-known-issues/
     cat 01-DNS-Record-hostname-not-found  ## Follow the steps one by one
     cat 02-corsPolicy  ## Follow the steps one by one
     cat 03-SSL  ## Follow the steps one by one
     cat 04-vaultFailedtoconnect  ## Follow the steps one by one
     cat 05-coreQUERY_CHECKS  ## Follow the steps one by one
 ```
 ### 2. Extra configuration for ISTIO & VirtualServices:
 - Go to ``` 07-VirtualService/local ``` path and and apply the file.
 ```bash
     cd ../07-VirtualService/local/
     kaf ps-security.yaml
     kaf ecc-frontend.yaml
     kaf ecc-core.yaml
     kaf central-vault-vault2.yaml
 ```
- Deployment will be completed here