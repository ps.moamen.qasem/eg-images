#!/bin/bash
## create Self-certifacte :
  openssl genrsa -out tls.key 2048 >> /dev/null 2>&1
  openssl req -new -key tls.key -out tls.csr -subj "/C=JO/ST=Amman/L=Amman/O=Awad/OU=MS/CN=*.progressoft.dev" >> /dev/null 2>&1
  openssl x509 -req -days 3650 -in tls.csr -signkey tls.key -out tls.crt >> /dev/null 2>&1
## apply TLS :
  kubectl -n istio-system create secret tls istio-ingressgateway-certs --key tls.key --cert tls.crt >> /dev/null 2>&1
### restart gateway
  echo "=========================="
  echo "Current running Istio pods status..."
  echo "=========================="
  kubectl get pods -n istio-system |grep istio | awk -F' ' '{print $1,$3}' |  column -t 
  echo "=========================="
  echo "Applied new Crt and Key now deleting the istio pods..."
   ## below commad will replace 3 pod with new pods.
  kubectl get pods -n istio-system |grep istio | awk -F' ' '{print $1}'  | tr '\n' ' ' | xargs kubectl delete pods -n istio-system
  sleep 10
  echo "Starting the new istio pods status..."
  echo "=========================="
  sleep 20
  kubectl get pods -n istio-system |grep istio | awk -F' ' '{print $1,$3}' |  column -t 
