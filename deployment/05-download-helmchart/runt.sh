#!/bin/bash
##########################################################################################################
# For Helm3 Kindly add the below harbor repo into your local system
 helm repo add --username <USERNAME> --password <CLI Secret> sierra https://harbor.progressoft.io/chartrepo/sierra
 helm repo add --username <USERNAME> --password <CLI Secret> genesis https://harbor.progressoft.io/chartrepo/genesis
 helm repo add --username <USERNAME> --password <CLI Secret> recipes https://harbor.progressoft.io/chartrepo/recipes
 helm repo add --username <USERNAME> --password <CLI Secret> payhub  https://harbor.progressoft.io/chartrepo/payhub

##########################################################################################################
## Remove the "bank-helm" directory in the current path so to create a new one.
rm -rf bank-helm

##########################################################################################################
## Update the dependancy for prompt-check
helm dependency update prompt-check

##########################################################################################################
## rename the download charts to "bank-helm"
mv prompt-check/charts bank-helm

##########################################################################################################
## remove the output file if exists 
rm -rf output

##########################################################################################################

# helm template master prompt-check\
#     --output-dir output  \
#     --values prompt-check/profiles/agricole.yaml
# tele build -o output/ayesh.tar payhub --values dev/profiles/dev.yaml --values dev/profiles/dev/ayesh.yaml
# helm repo add --username <USERNAME> --password <CLI Secret> sierra https://harbor.progressoft.io/chartrepo/sierra
# helm repo add --username <USERNAME> --password <CLI Secret> genesis https://harbor.progressoft.io/chartrepo/genesis
# helm repo add --username <username> --password <password> recipes https://harbor.progressoft.io/chartrepo/recipes
# helm repo update
# helm repo add --username <USERNAME> --password <CLI Secret> sierra https://harbor.progressoft.io/chartrepo/sierra
# helm repo add --username <USERNAME> --password <CLI Secret> sierra https://harbor.progressoft.io/chartrepo/core
# helm repo add --username <USERNAME> --password <CLI Secret> sierra https://harbor.progressoft.io/chartrepo/sierra
# helm repo add --username <USERNAME> --password <CLI Secret> sierra https://harbor.progressoft.io/chartrepo/payhub
# helm repo add --username <USERNAME> --password <CLI Secret> payhub  https://harbor.progressoft.io/chartrepo/payhub
# helm repo add --username <USERNAME> --password <CLI Secret> recipes  https://harbor.progressoft.io/chartrepo/recipes
#helm upgrade --install ecc prompt-check --timeout 1200s --create-namespace --namespace ecc -f prompt-check/profiles/agricole.yaml


##########################################################################################################
## To deploy the propmt-check webapp we need database connection and dbschema.
## If you are using the  public one then use that Ip otherwise (Create a new db) follow the below command.
## If you have running database in the docker then skip this 1st step and move forward.
docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=P@ssw0rd" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2022-latest

## run this on test just to create Schema auto without manual steps :
k apply -f dbschema.yaml
## create 2 database on MSSQL with name : ecc_db and ecc_sec.
## follow the steps to create a database.
docker exec -it <CONTAINER ID>  bash # Enter
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P P@ssw0rd # Enter
CREATE DATABASE ecc_db; # Enter
go # Enter
CREATE DATABASE ecc_sec;  # Enter
go  # Enter
# You can validate is the database is created successfully to run the below command.
SELECT Name from sys.databases; # Enter
go # Enter
## Fetch the database IPAddress and then update it in the prompt-check/profiles/agricole.yaml
docker inspect <CONTAINER ID>  |grep IPAddress

##########################################################################################################
## update the image tag and database paramter with IPAddress, vault secert and the others on this file prompt-check/profiles/agricole.yaml
## Example:
                # prompt_checkTag: 230522-05d2568b 
                # vault_token: hvs.dAjgyi3V12gHxz2oGgTehSBp
                # db_url: jdbc:sqlserver://;serverName=172.17.0.2;databaseName=ecc_db;encrypt=false;sendStringParametersAsUnicode=false
                # host: "172.17.0.2"

cd bank-helm/
tar zxvf prompt-check*.tgz
rm -rf prompt-check*.tgz
cp -rp  ../prompt-check/profiles  prompt-check
##mv  prompt-check/* ../
helm upgrade --install ecc prompt-check --timeout 1200s --create-namespace --namespace ecc -f prompt-check/profiles/agricole.yaml

## to test mock service update below param : isCheckTypeMandatory=0 , EnableFetchMicrCurrencyMap=0 
## update table parameters set prm_value=0 where prm_key='isCheckTypeMandatory';
## update table parameters set prm_value=0 where prm_key='EnableFetchMicrCurrencyMap';