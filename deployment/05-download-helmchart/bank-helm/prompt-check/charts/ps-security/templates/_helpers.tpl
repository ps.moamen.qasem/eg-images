{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ps-security.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
*/}}
{{- define "ps-security.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ps-security.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ps-security.labels" -}}
helm.sh/chart: {{ include "ps-security.chart" . }}
{{ include "ps-security.selectorLabels" . }}
app.kubernetes.io/version: {{ .Values.image.tag | default .Chart.AppVersion | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ps-security.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ps-security.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "ps-security.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ps-security.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the service DNS name.
*/}}
{{- define "ps-security.serviceDnsName" -}}
{{ include "ps-security.fullname" . }}-headless.{{ .Release.Namespace }}.svc.{{ .Values.clusterDomain }}
{{- end }}

{{- define "ps-security.databasePasswordEnv" -}}
- name: KC_DB_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.database.existingSecret | default (printf "%s-database" (include "ps-security.fullname" . ))}}
      key: {{ .Values.database.existingSecretKey | default "password" }}
{{- end -}}

{{/*
Create database username derived from shortname and release name
*/}}
{{- define "ps-security.database.username" -}}
{{- if ((.Values.database).username) }}
{{- .Values.database.username }}
{{- else }}
{{- printf "%s_sec" (.Release.Name | replace "-" "" | trunc 20) }}
{{- end }}
{{- end }}

{{/*
Create database password derived from shortname and release name
*/}}
{{- define "ps-security.database.password" -}}
{{- if ((.Values.database).password) }}
{{- .Values.database.password }}
{{- else }}
{{- printf "%s_sec" (.Release.Name | replace "-" "" | trunc 20) }}
{{- end }}
{{- end }}