{{/*
Expand the name of the chart.
*/}}

{{- define "library.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "library.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "library.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "library.labels" -}}
helm.sh/chart: {{ include "library.chart" . }}
{{ include "library.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "library.selectorLabels" -}}
app.kubernetes.io/name: {{ include "library.name" . }}
app.kubernetes.io/shortname: {{ include "library.shortname" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "library.serviceAccountName" -}}
{{- if .Values.serviceAccount }}
{{- if .Values.serviceAccount.create }}
{{- default (.Chart.Name) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
{{- end }}


{{/*
Default Image Repository
*/}}
{{- define "library.imageRepository" -}}
{{- if .Values.image }}
{{- default .Chart.Name (tpl .Values.image.repository .) }}
{{- else }}
{{- .Chart.Name }}
{{- end }}
{{- end }}


{{/*
Default Tag Variable Name
*/}}
{{- define "library.imageTagVar" -}}
{{- if .Values.global }}
{{- tpl ( printf "{{ tpl .Values.global.%s_tag . }}" .Chart.Name | replace "-" "_" ) . }}
{{- end }}
{{- end }}

{{/*
Default Image Tag
*/}}
{{- define "library.imageTag" -}}
{{ default .Chart.AppVersion ( default ( default ((.Values.global).systemTag) ((.Values.image).tag) ) (include "library.imageTagVar" .) ) }}
{{- end }}

{{/*
Default Image Pull Policy
*/}}
{{- define "library.imagePullPolicy" -}}
{{- if ((.Values.image).pullPolicy) }}
{{- .Values.image.pullPolicy }}
{{- else }}
{{- "IfNotPresent" }}
{{- end }}
{{- end }}

{{/*
Default Image Pull Secret
*/}}
{{- define "library.imagePullSecrets" -}}
{{- if  .Values.imagePullSecrets }}
{{- toYaml .Values.imagePullSecrets }}
{{- else if  ((.Values.global).systemPullSecrets) }}
{{- toYaml .Values.global.systemPullSecrets }}
{{- end }}
{{- end }}




{{/*
Create chart short name
*/}}
{{- define "library.shortname" -}}
{{- $name := .Chart.Name | replace "-" "" | replace "_" "" }}
{{- if ge (len $name) 6 }}
{{- $name = $name | replace "a" "" | replace "e" "" }}
{{- if ge (len $name) 6 }}
{{- $name = $name | replace "i" "" | replace "o" "" }} 
{{- if ge (len $name) 6 }}
{{- $name | replace "u" "" | replace "y" "" | trunc 4 }}
{{- else }}
{{- $name | trunc 4 }}
{{- end }}
{{- else }}
{{- $name | trunc 4 }}
{{- end }}
{{- else }}
{{- $name | trunc 4 }}
{{- end }}
{{- end }}


{{/*
Create database username derived from shortname and release name
*/}}
{{- define "library.database.username" -}}
{{- if ((.Values.database).username) }}
{{- .Values.database.username }}
{{- else }}
{{- printf "%s_%s" (.Release.Name | replace "-" "" | trunc 20) (include "library.shortname" .) }}
{{- end }}
{{- end }}


{{/*
Create database password derived from shortname and release name
*/}}
{{- define "library.database.password" -}}
{{- if ((.Values.database).password) }}
{{- .Values.database.password }}
{{- else }}
{{- printf "%s_%s" (.Release.Name | replace "-" "" | trunc 20) (include "library.shortname" .) }}
{{- end }}
{{- end }}
