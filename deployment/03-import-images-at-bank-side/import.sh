#!/bin/bash
#################################################################################
# from you PC : add the node ip ranges
#################################################################################
# change IP for Nodes host:
IP=("192.168.56.11")
# k3s ctr images import /var/lib/rancher/k3s/agent/images/images.tar

# copy tar image to : /var/lib/rancher/k3s/agent/images/
# we can restart the k3s service on each node the images will be cached
# ```bash
# systemctl restart k3s
# sleep 20
# until nc -z -v -w30 127.0.0.1 6443;do sleep 5;done

for i in ${IP[@]}
 do
  ssh  vagrant@$i 'sudo chmod 777 -R  /var/lib/rancher/k3s/'
  scp ../images.tar vagrant@$i:/var/lib/rancher/k3s/agent/images/
  ssh  vagrant@$i 'sudo k3s ctr images import /var/lib/rancher/k3s/agent/images/images.tar' > /tmp/imagePullOutPut.txt
  if [ $? == 0 ] 
   then
    echo "=============================="
    echo "Images are imported successfully on $i "
    echo "=============================="
    bash output.sh
    ssh  vagrant@$i 'systemctl restart k3s.service >> /dev/null 2>&1 
       sleep 20
       until nc -z -v -w30 127.0.0.1 6443;do sleep 5;done
       exit'
    else
    echo "=============================="
    echo "Failed to import images on $i "
    echo "=============================="
  fi
 done
#################################################################################
## or 
#################################################################################